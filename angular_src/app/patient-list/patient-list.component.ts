import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { Patient } from '../model/patient';
import { PatientService } from '../service/patient.service';
import { PatientEditComponent } from '../patient-edit/patient-edit.component';
import { User } from '../model/user';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import { AddPatientComponent } from '../add-patient/add-patient.component';
import { DelPatientComponent } from '../del-patient/del-patient.component';
import { MedplanEditComponent } from '../medplan-edit/medplan-edit.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  patients: Patient[];
  list: MatTableDataSource<any>;
  id_user: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private patientService: PatientService,
    private dialog: MatDialog,
    iconRegistry: MatIconRegistry, 
    sanitizer: DomSanitizer 
  ) { iconRegistry.addSvgIcon(
    'thumbs-up',
    sanitizer.bypassSecurityTrustResourceUrl('assets/add-user-button.svg'));
    }
  displayedColumns: string[] = ['id_patient', 'name', 'gender', 'birthday', 'email','actions'];

  ngOnInit() {
    this.route.params.forEach((urlParams) => {
      this.id_user = urlParams.id_user;
    });

    this.patientService.getAllPatients(this.id_user).subscribe(
      response => {this.patients = response; this.list = new MatTableDataSource(response);
                   this.list.paginator = this.paginator; }
      );
  }

  openDialog(patient: Patient): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(PatientEditComponent, {
      data: patient
    });
  }

  openDialogMed(patient: Patient): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(MedplanEditComponent, {
      data: patient
    });
  }

  openDialogAdd(): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(AddPatientComponent,{
      data: {id_user: this.id_user}
    });
  }

  openDialogDelete(id: number): void{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(DelPatientComponent,{
      data: {id_patient: id}
    });
  }

  caregiverList(){
    this.router.navigate([this.id_user + '/doctor/caregiver-list']);
  }

  medicationList(){
    this.router.navigate([this.id_user + '/doctor/medication-list']);
  }

  medplanList(){
    this.router.navigate([this.id_user + '/doctor/med-plan']);
  }

}
