import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog, MatDialogConfig } from '@angular/material';
import { Patient } from '../model/patient';
import { PatientService } from '../service/patient.service';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  patients: Patient[];
  list: MatTableDataSource<any>;
  id_user: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private patientService: PatientService,
    private dialog: MatDialog,
  ) { }

  displayedColumns: string[] = ['id_patient', 'name', 'gender', 'birthday', 'email'];

  ngOnInit() {
    this.route.params.forEach((urlParams) => {
      this.id_user = urlParams.id_user;
    });

    this.patientService.getPatientsByCaregiver(this.id_user).subscribe(
      response => {this.patients = response; this.list = new MatTableDataSource(response);
                   this.list.paginator = this.paginator; }
      );
  }

}
